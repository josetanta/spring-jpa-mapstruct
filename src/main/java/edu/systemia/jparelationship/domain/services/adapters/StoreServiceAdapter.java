package edu.systemia.jparelationship.domain.services.adapters;

import java.util.List;

import org.springframework.stereotype.Service;

import edu.systemia.jparelationship.domain.services.StoreService;
import edu.systemia.jparelationship.infrastructure.dtos.CommentDTO;
import edu.systemia.jparelationship.infrastructure.dtos.ProductDTO;
import edu.systemia.jparelationship.infrastructure.mappers.CommentMapper;
import edu.systemia.jparelationship.infrastructure.mappers.ProductMapper;
import edu.systemia.jparelationship.infrastructure.persistente.repository.CommentRepository;
import edu.systemia.jparelationship.infrastructure.persistente.repository.ProductRepository;
import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class StoreServiceAdapter implements StoreService {

	private final ProductRepository productRepository;
	private final ProductMapper productMapper;

	private final CommentRepository commentRepository;
	private final CommentMapper commentMapper;

	@Override
	public List<ProductDTO> findAllProducts() {
		var result = productRepository.findAll();

		return productMapper.toDTO(result);
	}

	@Override
	public List<CommentDTO> findAllComments() {
		var result = commentRepository.findAll();
		return commentMapper.toDTO(result);
	}

}
