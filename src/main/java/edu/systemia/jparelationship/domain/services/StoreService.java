package edu.systemia.jparelationship.domain.services;

import java.util.List;

import edu.systemia.jparelationship.infrastructure.dtos.CommentDTO;
import edu.systemia.jparelationship.infrastructure.dtos.ProductDTO;

/**
 * Service for business logic from storing of product
 * 
 * @author josetanta
 */
public interface StoreService {

	/**
	 * Find all products from DATABASE and map to DTO
	 * 
	 * @return the list of ProductDTO
	 */
	List<ProductDTO> findAllProducts();

	/**
	 * Find all comments from DATABASE and map to DTO
	 * 
	 * @return the list of CommentDTO
	 */
	List<CommentDTO> findAllComments();
}
