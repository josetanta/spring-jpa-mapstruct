package edu.systemia.jparelationship;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JparelationshipApplication {

	public static void main(String[] args) {
		SpringApplication.run(JparelationshipApplication.class, args);
	}

}
