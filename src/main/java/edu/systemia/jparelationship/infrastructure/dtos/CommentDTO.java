package edu.systemia.jparelationship.infrastructure.dtos;

import java.io.Serializable;

public record CommentDTO(
	Long commentId,
	String description,
	ProductDTO product) implements Serializable {

}
