package edu.systemia.jparelationship.infrastructure.dtos;

import java.io.Serializable;
import java.util.List;

public record ProductDTO(
	Long id,
	String name,
	List<CommentDTO> comments) implements Serializable {

}
