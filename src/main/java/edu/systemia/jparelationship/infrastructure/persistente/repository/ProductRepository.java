package edu.systemia.jparelationship.infrastructure.persistente.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.systemia.jparelationship.infrastructure.persistente.entities.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

}
