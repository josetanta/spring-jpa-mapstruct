package edu.systemia.jparelationship.infrastructure.persistente.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.systemia.jparelationship.infrastructure.persistente.entities.Comment;

public interface CommentRepository extends JpaRepository<Comment, Long> {

}
