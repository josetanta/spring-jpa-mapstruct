package edu.systemia.jparelationship.infrastructure.mappers;

import java.util.List;

import org.mapstruct.Mapper;

import edu.systemia.jparelationship.infrastructure.dtos.ProductDTO;
import edu.systemia.jparelationship.infrastructure.persistente.entities.Product;

@Mapper(componentModel = "spring", uses = { CommentMapper.class })
public interface ProductMapper {

	ProductDTO toDTO(Product entity);

	List<ProductDTO> toDTO(List<Product> entities);

}
