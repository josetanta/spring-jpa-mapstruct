package edu.systemia.jparelationship.infrastructure.mappers;

import java.util.List;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import edu.systemia.jparelationship.infrastructure.dtos.CommentDTO;
import edu.systemia.jparelationship.infrastructure.persistente.entities.Comment;

@Mapper(componentModel = "spring")
public interface CommentMapper {

	@Mapping(source = "body", target = "description")
	@Mapping(source = "id", target = "commentId")
	@Mapping(source = "product", target = "product", ignore = true)
	CommentDTO toDTO(Comment entity);

	List<CommentDTO> toDTO(List<Comment> entities);

	@Mapping(source = "description", target = "body")
	@Mapping(source = "commentId", target = "id")
	@Mapping(source = "product", target = "product", ignore = true)
	Comment toEntity(CommentDTO dto);

	List<Comment> toEntity(List<CommentDTO> dtos);

}
