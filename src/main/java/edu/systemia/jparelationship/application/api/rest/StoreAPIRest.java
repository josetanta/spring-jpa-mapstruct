package edu.systemia.jparelationship.application.api.rest;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import edu.systemia.jparelationship.domain.services.StoreService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
@RequiredArgsConstructor
public class StoreAPIRest {

	private final StoreService storeService;

	@GetMapping("/products")
	public ResponseEntity<Object> getAllProducts() {
		log.info("searching all products");
		var result = storeService.findAllProducts();
		return ResponseEntity.ok(result);
	}

	@GetMapping("/comments")
	public ResponseEntity<Object> getAllComments() {
		log.info("searching all products");
		var result = storeService.findAllComments();
		return ResponseEntity.ok(result);
	}
}
